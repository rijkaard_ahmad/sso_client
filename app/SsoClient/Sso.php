<?php

namespace App\SsoClient;

class Sso 
{
	public static $result;
	public static $request;
	static $fails;
	public function __construct($post)
	{
		Self::$request = $post;
		Self::$fails   = false;
		Self::Curl();
	}

	public function Curl()
	{
		$curl=curl_init();
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($curl,CURLOPT_URL,env('SSO_URL'));
		curl_setopt($curl,CURLOPT_POST,1);
		curl_setopt($curl,CURLOPT_POSTFIELDS,http_build_query(Sso::$request));
		curl_setopt($curl,CURLOPT_TIMEOUT,20);
		curl_setopt($curl,CURLOPT_HTTPHEADER, array(
		    'Accept: application/json')
		);
		$exec=curl_exec($curl);
		if(!$exec)
		{
			return Sso::$fails = true;
		}
		curl_close($curl);
		$result = json_decode($exec);
		Sso::$result = $result;
	}

	public function CasLogin($request)
	{
		Sso::$request = $request;
	}

	public function UserData()
	{
		return Sso::$result;
	}

	public function Fails()
	{
		if(Sso::$fails == true)
		{
			return true;
		}
		return Sso::$result->error;
	}

	public function Check()
	{
		if(Sso::$fails == true)
		{
			return false;
		}
		return !Sso::$result->error;
	}

	public function Message()
	{
		if(Sso::$fails == true)
		{
			return [0 => [0 => 'Connection Error, Please Check Your Configurations']];
		}
		return Sso::$result->messages;
	}

	public function SsoApp()
	{
		return Sso::$result->data->sso_app;
	}

	public function User()
	{
		return Sso::$result->data->user;
	}

	public function Role()
	{
		return Sso::$result->data->role;
	}

	public function Permission()
	{
		return Sso::$result->data->permission;
	}
}

