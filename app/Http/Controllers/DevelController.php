<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SsoClient\Sso;
use Ahmadlab\Karawang\Karawang;
use Ahmadlab\Cas\Cas;
use Redirect;

class DevelController extends Controller
{

    public function Login()
    {
    	$email 			= "ahmadnorin@gmail.com";
    	$password       = "karawang";

    	$post = [
    	'email'			=> $email,
    	'password'		=> $password,
    	'appid'			=> env('SSO_APPID'),
    	'secret'		=> env('SSO_SECRET'),
    	'callback'		=> env('SSO_CALLBACK')
    	];

    	$data = new Cas();
        $data->Attempt($post);
	   	if($data->Fails())
	   	{
            dd($data->message());
	   	}
	   	else
	   	{
	   		//dd($data->Token());
            $profile = $data->profile($data->token());
            dd($profile);
	   	}
    }

    public function cas(Request $request)
    {
        $data = new Cas;
        $app = [
        'appid'         => env('SSO_APPID'),
        'secret'        => env('SSO_SECRET'),
        'callback'      => env('SSO_CALLBACK')
        ];
        return $data->cas($app);
    }

    public function callback(Request $request)
    {
        $data = new Cas();

        //dd($data->Token());
        $profile = $data->profile($request->get('token'));
        dd($profile);
    }
}
