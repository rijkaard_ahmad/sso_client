<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', 'DevelController@Login');
Route::get('cas', 'DevelController@cas');
//Route::get('cas', 'DevelController@cas')->middleware('Cas');
Route::get('aku', 'DevelController@aku');
Route::get('callback', 'DevelController@callback');
